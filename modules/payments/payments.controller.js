const httpStatus = require('http-status');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const paymentService = require('./payments.service');
const shopService = require('../shops/shops.service');

const createPayment = catchAsync(async (req, res) => {
    const { shop: shopId, services, products } = req.body;

    const shop = await shopService.getShopById(shopId);
    if (!shop) {
        throw new ApiError(httpStatus.BAD_REQUEST, `Shop with id=${shopId} doesn't found`);
    }

    if (
        services.some(s => !shop.services.includes(s)) ||
        products.some(p => !shop.goods.includes(p.name))
    ) {
        throw new ApiError(httpStatus.BAD_REQUEST, `Payment is not saved. Invalid services or products list`);
    }

    const probability = n => !!n && Math.random() <= n;
    const status = probability(0.7) ? 'success' : 'failed';

    const payment = await paymentService.createPayment({...req.body, status });
    res.status(httpStatus.CREATED).send(payment);
});

const getPayments = catchAsync(async (req, res) => {
    const result = await paymentService.queryPayments();
    res.send(result);
});

const getPayment = catchAsync(async (req, res) => {
    const payment = await paymentService.getPaymentById(req.params.paymentId);
    if (!payment) {
        throw new ApiError(httpStatus.NOT_FOUND, 'Payment not found');
    }
    res.send(payment);
});

const updatePayment = catchAsync(async (req, res) => {
    const payment = await paymentService.updatePaymentById(req.params.paymentId, req.body);
    res.send(payment);
});

const deletePayment = catchAsync(async (req, res) => {
    await paymentService.deletePaymentById(req.params.paymentId);
    res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
    createPayment,
    getPayments,
    getPayment,
    updatePayment,
    deletePayment,
};
