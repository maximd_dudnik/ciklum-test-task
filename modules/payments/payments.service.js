const httpStatus = require('http-status');
const Payment = require('./payments.model');
const ApiError = require('../../utils/ApiError');

const createPayment = async (paymentBody) => {
    const payment = await Payment.create(paymentBody);
    return payment;
};

const queryPayments = async () => {
    const payments = await Payment.find();
    return payments;
};

const getPaymentById = async (id) => {
    return Payment.findById(id);
};

const updatePaymentById = async (paymentId, updateBody) => {
    const payment = await getPaymentById(paymentId);
    if (!payment) {
        throw new ApiError(httpStatus.NOT_FOUND, 'Payment not found');
    }
    Object.assign(payment, updateBody);
    await payment.save();
    return payment;
};

const deletePaymentById = async (paymentId) => {
    const payment = await getPaymentById(paymentId);
    if (!payment) {
        throw new ApiError(httpStatus.NOT_FOUND, 'Payment not found');
    }
    await payment.remove();
    return payment;
};

module.exports = {
    createPayment,
    queryPayments,
    getPaymentById,
    updatePaymentById,
    deletePaymentById,
};
