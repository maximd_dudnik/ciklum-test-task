const express = require('express');
const validate = require('../../middlewares/validate');

const paymentsValidation = require('./payments.validation');
const paymentsController = require('./payments.controller');

const router = express.Router();

router
    .route('/')
    .post(validate(paymentsValidation.createPayment), paymentsController.createPayment)
    .get(paymentsController.getPayments);

router
    .route('/:paymentId')
    .get(paymentsController.getPayment)
    .put(paymentsController.updatePayment)
    .delete(paymentsController.deletePayment);

module.exports = router;
