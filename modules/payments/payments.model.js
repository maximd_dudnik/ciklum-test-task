const mongoose = require('mongoose');
const toJSON = require('../../utils/plugins/toJSON');

const paymentSchema = mongoose.Schema(
    {
        number: String,
        date: Date,
        shop: String,
        products: [{
            name : String,
            quantity : Number
        }],
        services: [String],
        price: Number,
        commission: Number,
        cardNumber: String,
        cardValidTill: String,
        cvv: Number,
        status: { type: String, enum: ['success', 'failed'] }
    },
    {
        timestamps: false,
        strict: false
    }
);

paymentSchema.plugin(toJSON);

const Payment = mongoose.model('Payment', paymentSchema);


module.exports = Payment;
