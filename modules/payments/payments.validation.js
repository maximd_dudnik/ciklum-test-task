const Joi = require('joi');
const { objectId, creditCard, paymentDate } = require('../../utils/customValidationRules');

/*
{
   "number":"fds78sdf8",
   "date": "23/10/2020 17:58:21",
   "shop": "5b3cbe67eff7723267e7b5c6",
   "products":[
      {
         "name":"tomatoes",
         "quantity":5
      }
   ],
   "services":[
      "delivery"
   ],
   "price":30,
   "commission": 1.60,
   "cardNumber": "1234567812341234",
   "cardValidTill": "10/21",
    "cvv": 123
}
*/

module.exports = {
    // POST /payments
    createPayment: {
        body: {
            number: Joi.string().required(),
            date: Joi.string().custom(paymentDate),
            shop: Joi.string().custom(objectId).required(),
            products: Joi.array().items(Joi.object({
                name: Joi.string().required(),
                quantity: Joi.number().integer().positive().allow(0)
            })).min(1).required(),
            services: Joi.array().items(Joi.string()).min(1).required(),
            price: Joi.number().required(),
            commission: Joi.number().precision(2),
            cardNumber: Joi.string().custom(creditCard).required(),
            cardValidTill: Joi.string().required(),
            cvv: Joi.number().required()
        }
    }
};
