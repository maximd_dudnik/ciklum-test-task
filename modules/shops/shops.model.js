const mongoose = require('mongoose');
const toJSON = require('../../utils/plugins/toJSON');

const shopSchema = mongoose.Schema(
    {
        name: String,
        goods: [String],
        services: [String],
        commission: {
            fixed: Number,
            percentage: Number
        },
    },
    {
        timestamps: false,
        strict: false
    }
);

shopSchema.plugin(toJSON);

const Shop = mongoose.model('Shop', shopSchema);


module.exports = Shop;
