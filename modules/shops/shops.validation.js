const Joi = require('joi');

/*
{
   "name":"Beans grocery2",
   "goods":[
      "tomatoes",
      "apples"
   ],
   "services":[
      "delivery"
   ],
   "commission":{
      "fixed":1,
      "percentage":2
   }
}
*/

module.exports = {
    // POST /shops
    createShop: {
        body: {
            name: Joi.string().required(),
            goods: Joi.array().items(Joi.string()).required(),
            services: Joi.array().items(Joi.string()).required(),
            commission: {
                fixed: Joi.number().required(),
                percentage: Joi.number().required()
            }
        }
    }
};
