const httpStatus = require('http-status');
const Shop = require('./shops.model');
const ApiError = require('../../utils/ApiError');

const createShop = async (shopBody) => {
    const shop = await Shop.create(shopBody);
    return shop;
};

const queryShops = async () => {
    const shops = await Shop.find();
    return shops;
};

const getShopById = async (id) => {
    return Shop.findById(id);
};

const updateShopById = async (shopId, updateBody) => {
    const shop = await getShopById(shopId);
    if (!shop) {
        throw new ApiError(httpStatus.NOT_FOUND, 'Shop not found');
    }
    Object.assign(shop, updateBody);
    await shop.save();
    return shop;
};

const deleteShopById = async (shopId) => {
    const shop = await getShopById(shopId);
    if (!shop) {
        throw new ApiError(httpStatus.NOT_FOUND, 'Shop not found');
    }
    await shop.remove();
    return shop;
};

module.exports = {
    createShop,
    queryShops,
    getShopById,
    updateShopById,
    deleteShopById,
};
