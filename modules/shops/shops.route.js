const express = require('express');
const validate = require('../../middlewares/validate');

const shopsValidation = require('./shops.validation');
const shopsController = require('./shops.controller');

const router = express.Router();

router
    .route('/')
    .post(validate(shopsValidation.createShop), shopsController.createShop)
    .get(shopsController.getShops);

router
    .route('/:shopId')
    .get(shopsController.getShop)
    .put(shopsController.updateShop)
    .delete(shopsController.deleteShop);

module.exports = router;
