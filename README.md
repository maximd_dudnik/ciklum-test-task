## 1. Prerequisites

Install mongodb on local machine and create `test` database.

## 2. Project setup
```
yarn install
```

### Start project
```
node index.js
```
