const moment = require('moment');

const objectId = (value, helpers) => {
    if (!value.match(/^[0-9a-fA-F]{24}$/)) {
        return helpers.message('"{{#label}}" must be a valid mongo id');
    }
    return value;
};

const creditCard = (value, helpers) => {
    // Accept only digits, dashes or spaces
    if (/[^0-9-\s]+/.test(value)) return false;

    // The Luhn Algorithm. It's so pretty.
    let nCheck = 0, bEven = false;
    value = value.replace(/\D/g, "");

    for (var n = value.length - 1; n >= 0; n--) {
        var cDigit = value.charAt(n),
            nDigit = parseInt(cDigit, 10);

        if (bEven && (nDigit *= 2) > 9) nDigit -= 9;

        nCheck += nDigit;
        bEven = !bEven;
    }

    if ((nCheck % 10) != 0) {
        return helpers.message('"{{#label}}" must be a valid credit card number')
    }

    return value;
};

const paymentDate = (value, helpers) => {
    const date = moment(value, 'DD-MM-YYYY HH:mm:ss');

    if (!date.isValid()) {
        return helpers.message('"{{#label}}" invalid date')
    }

    if (date <= moment().subtract(10,'minutes')) {
        return helpers.message('"{{#label}}" expired date')
    }

    return date;
};

module.exports = {
    objectId,
    creditCard,
    paymentDate
};
