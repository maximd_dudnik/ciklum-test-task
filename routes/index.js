const express = require('express');
const shopsRoute = require('../modules/shops/shops.route');
const paymentsRoute = require('../modules/payments/payments.route');

const router = express.Router();

router.use('/shops', shopsRoute);
router.use('/payments', paymentsRoute);

module.exports = router;
